import { Environment } from '@abp/ng.core';

const baseUrl = 'http://localhost:4200';

export const environment = {
  production: false,
  application: {
    baseUrl,
    name: 'AppCitasDemoABP',
    logoUrl: '',
  },
  oAuthConfig: {
    issuer: 'https://localhost:44311',
    redirectUri: baseUrl,
    clientId: 'AppCitasDemoABP_App',
    responseType: 'code',
    scope: 'offline_access openid profile role email phone AppCitasDemoABP',
  },
  apis: {
    default: {
      url: 'https://localhost:44311',
      rootNamespace: 'AppCitasDemoABP',
    },
  },
} as Environment;
