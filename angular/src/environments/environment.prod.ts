import { Environment } from '@abp/ng.core';

const baseUrl = 'http://localhost:4200';

export const environment = {
  production: true,
  application: {
    baseUrl,
    name: 'AppCitasDemoABP',
    logoUrl: '',
  },
  oAuthConfig: {
    issuer: 'https://localhost:44311',
    redirectUri: baseUrl,
    clientId: 'AppCitasDemoABP_App',
    responseType: 'code',
    scope: 'offline_access AppCitasDemoABP',
  },
  apis: {
    default: {
      url: 'https://localhost:44311',
      rootNamespace: 'AppCitasDemoABP',
    },
  },
} as Environment;
