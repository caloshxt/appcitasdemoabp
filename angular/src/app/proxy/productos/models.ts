import type { ProductType } from '../app/productos/product-type.enum';
import type { EntityDto } from '@abp/ng.core';

export interface CreateProductoDto {
  name?: string;
  price: number;
  quantity: number;
  productType: ProductType;
}

export interface ProductoDto extends EntityDto<number> {
  name?: string;
  price: number;
  quantity: number;
  productType: ProductType;
}
