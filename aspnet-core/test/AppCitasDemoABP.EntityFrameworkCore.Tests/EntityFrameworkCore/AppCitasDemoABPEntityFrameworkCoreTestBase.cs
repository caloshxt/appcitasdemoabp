﻿using Volo.Abp;

namespace AppCitasDemoABP.EntityFrameworkCore
{
    public abstract class AppCitasDemoABPEntityFrameworkCoreTestBase : AppCitasDemoABPTestBase<AppCitasDemoABPEntityFrameworkCoreTestModule> 
    {

    }
}
