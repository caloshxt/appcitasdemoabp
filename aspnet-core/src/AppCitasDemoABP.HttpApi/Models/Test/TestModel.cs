﻿using System;

namespace AppCitasDemoABP.Models.Test
{
    public class TestModel
    {
        public string Name { get; set; }

        public DateTime BirthDate { get; set; }
    }
}