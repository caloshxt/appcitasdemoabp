﻿using AppCitasDemoABP.Categorias.Dtos;
using ReservaCitas.App.Categorias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace AppCitasDemoABP.Categorias
{
    public class CategoriaAppService : CrudAppService<Categoria, CategoriaDto, int, PagedAndSortedResultRequestDto, CreateCategoriaDto>
    {
        public CategoriaAppService(IRepository<Categoria, int> repository) : base(repository)
        {
        }
    }
}
