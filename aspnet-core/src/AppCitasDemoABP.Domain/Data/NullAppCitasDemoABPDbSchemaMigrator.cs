﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace AppCitasDemoABP.Data
{
    /* This is used if database provider does't define
     * IAppCitasDemoABPDbSchemaMigrator implementation.
     */
    public class NullAppCitasDemoABPDbSchemaMigrator : IAppCitasDemoABPDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}