﻿using AppCitasDemoABP.Users;
using ReservaCitas.App.Lugares;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace ReservaCitas.App.Correos
{
    public class Correo : Entity<int>
    {
        public string Mail { get; set; }
        public int Tipo { get; set; }
        public bool Verificado { get; set; }

        [ForeignKey("UserId")]
        public Guid? UsuarioId { set; get; }
        public AppUser Usuario { get; set; }

        [ForeignKey("Lugar")]
        public int? LugarId { get; set; }
        public  Lugar Lugar { get; set; }

    }
}
