﻿
using Volo.Abp.Domain.Entities;

namespace AppCitasDemoABP.App.Productos
{
    public class Producto: Entity<int>
    {
        public string Name { set; get; }
        public double Price { set; get; }
        public int Quantity { set; get; }
        public ProductType ProductType { set; get; }
    }

    public enum ProductType
    {
        Tipo1 = 1,
        Tipo2 = 2,
        Tipo3 = 3
    }
}
