﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

using Volo.Abp.Domain.Entities;

namespace ReservaCitas.App.Notificaciones
{
    public class Notificacion: Entity<int>
    {
        public string Mensaje { set; get; }
        public DateTime Fecha { set; get; }

        [ForeignKey("ReservaCita")]
        public int? ReservaCitaId;
    }
}
