﻿
using Volo.Abp.Domain.Entities;

namespace ReservaCitas.App.Direcciones
{
    public class Direccion : Entity<int>
    {
        public int Tipo { get; set; }

        public int? PaisId { get; set; }
        public int? ProvinciaId { get; set; }
        public int? CantonId { get; set; }
        public int? ParroquiaId { get; set; }
        public int? Zona { get; set; }
        public string CallePrincipal { get; set; }
        public string? CalleSecundaria { get; set; }
        public string? Barrio { get; set; }
        public string? Edificio { get; set; }
        public int? Numero { get; set; }
    }
}
