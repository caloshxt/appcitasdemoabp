﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ReservaCitas.App.Servicios;
using Volo.Abp.Domain.Entities;
using AppCitasDemoABP.Users;

namespace ReservaCitas.App.Categorias
{
    public class Categoria: Entity<int>
    {
        public string Nombre { set; get; }
        public string Descripcion { set; get; }
        public bool IsActive { set; get; }
        public bool IsSector { set; get; }

        [InverseProperty("Categorias")]
        public virtual ICollection<AppUser> Usuarios { get; set; }

        public int? CategoriaId { get; set; }
        [ForeignKey("CategoriaId")]
        public virtual Categoria Parent { get; set; }

        [InverseProperty("Categorias")]
        public virtual List<CatalogoServicio> CatalogoServicio { get; set; }

        [InverseProperty("Categorias")]
        public virtual ICollection<Servicio> Servicios { get; set; }

        public Categoria()
        {
            this.CatalogoServicio = new List<CatalogoServicio>();
        }

    }
}
