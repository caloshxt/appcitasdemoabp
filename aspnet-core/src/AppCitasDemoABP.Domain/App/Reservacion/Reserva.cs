﻿using AppCitasDemoABP.Users;
using ReservaCitas.App.Lugares;
using ReservaCitas.App.Servicios;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace ReservaCitas.App.Reservacion
{
    public class ReservaCita: Entity<int>
    {
        [ForeignKey("Usuario")]
        public Guid UsuarioId { set; get; }
        public AppUser Usuario { get; set; }

        [ForeignKey("Especialista")]
        public Guid EspecilistaId { set; get; }
        public AppUser Especialista { get; set; }

        public string Nombre { set; get; }
        public DateTime Fecha { set; get; }
        public string HoraInicio { set; get; }
        public string HoraFin { set; get; }
        public string Duracion { set; get; }
        public string Status { set; get; }

        [ForeignKey("TipoReserva")]
        public int TipoReservaId { set; get; }
        public TipoReserva TipoReserva { set; get; }

        [ForeignKey("Lugar")]
        public int LugarId { set; get; }
        public Lugar Lugar { set; get; }

        [ForeignKey("Servicio")]
        public int ServicioId { set; get; }
        public Servicio Servicio { set; get; }
    }

    public class TipoReserva: Entity<int>
    {
        [MaxLength(50)]
        public string Nombre { set; get; }
        public string Descripcion { set; get; }
    }
}
