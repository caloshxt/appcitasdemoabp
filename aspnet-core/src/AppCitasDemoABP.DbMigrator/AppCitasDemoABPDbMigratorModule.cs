﻿using AppCitasDemoABP.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace AppCitasDemoABP.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(AppCitasDemoABPEntityFrameworkCoreDbMigrationsModule),
        typeof(AppCitasDemoABPApplicationContractsModule)
        )]
    public class AppCitasDemoABPDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
