﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace AppCitasDemoABP
{
    [Dependency(ReplaceServices = true)]
    public class AppCitasDemoABPBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "AppCitasDemoABP";
    }
}
