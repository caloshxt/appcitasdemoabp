﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using AppCitasDemoABP.Data;
using Volo.Abp.DependencyInjection;

namespace AppCitasDemoABP.EntityFrameworkCore
{
    public class EntityFrameworkCoreAppCitasDemoABPDbSchemaMigrator
        : IAppCitasDemoABPDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public EntityFrameworkCoreAppCitasDemoABPDbSchemaMigrator(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            /* We intentionally resolving the AppCitasDemoABPMigrationsDbContext
             * from IServiceProvider (instead of directly injecting it)
             * to properly get the connection string of the current tenant in the
             * current scope.
             */

            await _serviceProvider
                .GetRequiredService<AppCitasDemoABPMigrationsDbContext>()
                .Database
                .MigrateAsync();
        }
    }
}