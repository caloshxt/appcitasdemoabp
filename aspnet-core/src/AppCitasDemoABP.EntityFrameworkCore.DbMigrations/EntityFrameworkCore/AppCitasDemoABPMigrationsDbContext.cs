﻿using AppCitasDemoABP.App.Productos;
using Microsoft.EntityFrameworkCore;
using ReservaCitas.App.Calendario;
using ReservaCitas.App.Categorias;
using ReservaCitas.App.Correos;
using ReservaCitas.App.Dependientes;
using ReservaCitas.App.Direcciones;
using ReservaCitas.App.Lugares;
using ReservaCitas.App.Notificaciones;
using ReservaCitas.App.Reservacion;
using ReservaCitas.App.Servicios;
using ReservaCitas.App.Telefonos;
using ReservaCitas.App.Usuarios;
using Volo.Abp.AuditLogging.EntityFrameworkCore;
using Volo.Abp.BackgroundJobs.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.FeatureManagement.EntityFrameworkCore;
using Volo.Abp.Identity;
using Volo.Abp.Identity.EntityFrameworkCore;
using Volo.Abp.IdentityServer.EntityFrameworkCore;
using Volo.Abp.PermissionManagement.EntityFrameworkCore;
using Volo.Abp.SettingManagement.EntityFrameworkCore;
using Volo.Abp.TenantManagement.EntityFrameworkCore;

namespace AppCitasDemoABP.EntityFrameworkCore
{
    /* This DbContext is only used for database migrations.
     * It is not used on runtime. See AppCitasDemoABPDbContext for the runtime DbContext.
     * It is a unified model that includes configuration for
     * all used modules and your application.
     */
    public class AppCitasDemoABPMigrationsDbContext : AbpDbContext<AppCitasDemoABPMigrationsDbContext>
    {
        public DbSet<Producto> Producto { get; set; }
        //
        public DbSet<TipoUsuario> TipoUsuario { set; get; }
        public DbSet<TipoIdentificacion> TipoIdentificacion { set; get; }
        //public DbSet<Usuario> Usuarios { set; get; }
        public DbSet<Categoria> Categorias { set; get; }
        public DbSet<Servicio> Servicios { set; get; }
        public DbSet<TipoServicio> TipoServicio { set; get; }
        public DbSet<Requisito> Requisito { set; get; }
        public DbSet<Lugar> Lugares { set; get; }
        public DbSet<TipoSemana> TipoSemana { set; get; }
        public DbSet<CalendarioEspecialista> CalendarioEspecialista { set; get; }
        public DbSet<JornadaDiaria> JornadaDiaria { set; get; }
        public DbSet<ItemLugar> ItemLugar { set; get; }
        public DbSet<ReservaCita> ReservaCita { set; get; }
        public DbSet<TipoReserva> TipoReserva { set; get; }
        public DbSet<Dependiente> Dependiente { set; get; }
        public DbSet<Notificacion> Notificacion { set; get; }
        public DbSet<CatalogoServicio> CatalogoServicio { set; get; }
        public DbSet<Telefono> Telefonos { set; get; }
        public DbSet<Correo> Correos { set; get; }
        public DbSet<Direccion> Direcciones { set; get; }

        public AppCitasDemoABPMigrationsDbContext(DbContextOptions<AppCitasDemoABPMigrationsDbContext> options) 
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            /* Include modules to your migration db context */

            builder.ConfigurePermissionManagement();
            builder.ConfigureSettingManagement();
            builder.ConfigureBackgroundJobs();
            builder.ConfigureAuditLogging();
            builder.ConfigureIdentity();
            builder.ConfigureIdentityServer();
            builder.ConfigureFeatureManagement();
            builder.ConfigureTenantManagement();

            /* Configure your own tables/entities inside the ConfigureAppCitasDemoABP method */

            builder.ConfigureAppCitasDemoABP();
        }
    }
}