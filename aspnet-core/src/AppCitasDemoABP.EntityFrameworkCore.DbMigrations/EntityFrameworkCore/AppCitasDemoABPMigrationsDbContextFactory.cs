﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace AppCitasDemoABP.EntityFrameworkCore
{
    /* This class is needed for EF Core console commands
     * (like Add-Migration and Update-Database commands) */
    public class AppCitasDemoABPMigrationsDbContextFactory : IDesignTimeDbContextFactory<AppCitasDemoABPMigrationsDbContext>
    {
        public AppCitasDemoABPMigrationsDbContext CreateDbContext(string[] args)
        {
            AppCitasDemoABPEfCoreEntityExtensionMappings.Configure();

            var configuration = BuildConfiguration();

            var builder = new DbContextOptionsBuilder<AppCitasDemoABPMigrationsDbContext>()
                .UseSqlServer(configuration.GetConnectionString("Default"));

            return new AppCitasDemoABPMigrationsDbContext(builder.Options);
        }

        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), "../AppCitasDemoABP.DbMigrator/"))
                .AddJsonFile("appsettings.json", optional: false);

            return builder.Build();
        }
    }
}
